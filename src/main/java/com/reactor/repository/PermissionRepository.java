package com.reactor.repository;

import com.reactor.entity.PermissionEntity;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

public interface PermissionRepository extends ReactiveCrudRepository<PermissionEntity, Long> {
}
