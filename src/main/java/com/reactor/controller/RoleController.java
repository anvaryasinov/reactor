package com.reactor.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(value = "/role")
public class RoleController {

    private boolean flag = false;

    @GetMapping(value = "/test1")
    public Mono<String> test() {
        return Mono.just("200")
                .map(integer -> {
                    while(true) {
                        if (flag) return integer;
                    }
                });
    }

    @GetMapping(value = "/test2")
    public Mono<String> test2() {
        return Mono.just("200");
    }

    @GetMapping(value = "/test3")
    public Mono<Void> test3() {
        flag = !flag;
        return Mono.empty();
    }


}
