package com.reactor.service.management;

import com.reactor.pojo.request.UserRequest;
import com.reactor.pojo.response.UserResponse;
import reactor.core.publisher.Mono;

public interface UserManagementService {
    Mono<UserResponse> get(Long id);
    Mono<UserResponse> add(UserRequest request);
    Mono<UserResponse> edit(UserRequest request);
    Mono<Void> remove(Long id);
}
