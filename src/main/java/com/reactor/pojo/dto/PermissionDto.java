package com.reactor.pojo.dto;

import com.reactor.pojo.model.PermissionModel;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PermissionDto {
    private final Long id;
    private final String name;

    public static PermissionDto fromModel(final PermissionModel model) {
        return PermissionDto.builder()
                .id(model.getId())
                .name(model.getName())
                .build();
    }
}
