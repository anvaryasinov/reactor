package com.reactor.pojo.request;

import com.reactor.pojo.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserRequest {
    private UserDto userDto;
    private Long roleId;
}
