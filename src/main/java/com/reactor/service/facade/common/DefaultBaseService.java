package com.reactor.service.facade.common;

import com.reactor.common.TimestampProvider;
import com.reactor.entity.BaseEntity;
import lombok.AllArgsConstructor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public abstract class DefaultBaseService<T extends BaseEntity, R> implements BaseService<T, R> {

    private final TimestampProvider timestampProvider;

    private final ReactiveCrudRepository<T, Long> repository;

    protected Mono<T> commitEntity(final T entity) {
        if (entity.getId() == null) {
            entity.setCreateDateTime(timestampProvider.getServerZonedDateTime().toString());
        } else entity.setUpdateDateTime(timestampProvider.getServerZonedDateTime().toString());
        return repository.save(entity);
    }

    @Override
    public Mono<T> findById(final Long id) {
        return repository.findById(id);
    }

    @Override
    public Mono<Void> deleteById(final Long id) {
        return repository.deleteById(id);
    }

}
