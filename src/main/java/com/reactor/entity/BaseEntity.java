package com.reactor.entity;

import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;

@Getter
public class BaseEntity {
    @Id
    private Long id;

    @Column("create_date_time")
    private String createDateTime;

    @Column("update_date_time")
    private String updateDateTime;

    public void setCreateDateTime(final String createDateTime) {
        this.createDateTime = createDateTime;
    }

    public void setUpdateDateTime(final String updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

}
