create table roles
(
    id               bigserial primary key,
    name             varchar(100) not null,
    create_date_time varchar(100),
    update_date_time varchar(100)
);

create table permissions
(
    id               bigserial primary key,
    name             varchar(100) not null,
    create_date_time varchar(100),
    update_date_time varchar(100)
);

create table role_permission_mappings
(
    id            bigserial primary key,
    role_id       bigint not null references roles (id),
    permission_id bigint not null references permissions (id)
);

create table users
(
    id               bigserial primary key,
    name             varchar not null,
    role_id          bigint  not null references roles (id),
    create_date_time varchar(100),
    update_date_time varchar(100)
);