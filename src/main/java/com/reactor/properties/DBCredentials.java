package com.reactor.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "database")
public class DBCredentials {
    private String url;
    private String username;
    private String password;
    private String dbName;
    private String port;
}
