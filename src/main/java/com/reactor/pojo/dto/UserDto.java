package com.reactor.pojo.dto;

import com.reactor.pojo.model.UserModel;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UserDto {
    private final Long id;
    private final String name;
    private final RoleDto roleDto;

    public static UserDto fromModel(final UserModel model) {
        return UserDto.builder()
        .id(model.getId())
        .name(model.getName())
        .roleDto(RoleDto.fromModel(model.getRole()))
        .build();
    }

}
