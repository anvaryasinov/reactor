package com.reactor.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("permissions")
@Getter
@Setter
@Builder
public class PermissionEntity extends BaseEntity {

    @Column("name")
    private String name;

}
