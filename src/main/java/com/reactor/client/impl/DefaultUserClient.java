package com.reactor.client.impl;

import com.reactor.client.UserClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Component
public class DefaultUserClient implements UserClient {

    private final WebClient webClient;

    @Autowired
    public DefaultUserClient(@Qualifier("userWebClient") final WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public Mono<String> getUserData() {
        return webClient.get()
                .uri(uriBuilder -> uriBuilder.path("/users").build())
                .exchange()
                .map(clientResponse -> String.valueOf(clientResponse.statusCode().value()));
    }

}
