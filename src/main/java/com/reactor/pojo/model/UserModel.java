package com.reactor.pojo.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UserModel {
    private final Long id;
    private final String name;
    private final RoleModel role;
}
