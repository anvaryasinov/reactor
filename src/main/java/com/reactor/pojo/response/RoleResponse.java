package com.reactor.pojo.response;

import com.reactor.pojo.dto.RoleDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class RoleResponse {
    private final RoleDto roleDto;
}
