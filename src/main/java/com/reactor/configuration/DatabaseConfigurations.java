package com.reactor.configuration;

import com.reactor.properties.DBCredentials;
import io.r2dbc.postgresql.PostgresqlConnectionConfiguration;
import io.r2dbc.postgresql.PostgresqlConnectionFactory;
import io.r2dbc.spi.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableR2dbcRepositories(basePackages = {"com.reactor.repository"})
public class DatabaseConfigurations extends AbstractR2dbcConfiguration {

    private final DBCredentials dbCredentials;

    @Autowired
    public DatabaseConfigurations(final DBCredentials dbCredentials) {
        this.dbCredentials = dbCredentials;
    }

    @Bean
    @Override
    public ConnectionFactory connectionFactory() {
        var configuration = PostgresqlConnectionConfiguration.builder()
                .host(dbCredentials.getUrl())
                .port(Integer.parseInt(dbCredentials.getPort()))
                .username(dbCredentials.getUsername())
                .password(dbCredentials.getPassword())
                .database(dbCredentials.getDbName())
                .build();
        return new PostgresqlConnectionFactory(configuration);
    }

}
