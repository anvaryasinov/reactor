package com.reactor.repository;

import com.reactor.entity.RolePermissionMappingEntity;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface RolePermissionMappingRepository extends ReactiveCrudRepository<RolePermissionMappingEntity, Long> {

    @Query("SELECT * FROM role_permission_mappings WHERE role_id = :roleId")
    Flux<RolePermissionMappingEntity> findByRoleId(Long roleId);

}
