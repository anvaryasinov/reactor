package com.reactor.service.facade.common;

import com.reactor.entity.BaseEntity;
import reactor.core.publisher.Mono;

public interface BaseService<T extends BaseEntity, R> {
    Mono<T> findById(Long id);
    Mono<R> saveOrUpdate(T entity);
    Mono<Void> deleteById(Long id);
}
