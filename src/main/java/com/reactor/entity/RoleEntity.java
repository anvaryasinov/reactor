package com.reactor.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("roles")
@Getter
@Setter
@Builder
public class RoleEntity extends BaseEntity {

    @Column("name")
    private String name;

}
