package com.reactor.client;

import reactor.core.publisher.Mono;

public interface UserClient {
    Mono<String> getUserData();
}
