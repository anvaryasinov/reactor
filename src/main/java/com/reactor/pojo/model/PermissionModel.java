package com.reactor.pojo.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PermissionModel {
    private final Long id;
    private final String name;
}
