package com.reactor.service.management;

import com.reactor.pojo.response.RoleResponse;
import reactor.core.publisher.Mono;

public interface RoleManagementService {
    Mono<RoleResponse> get(Long id);
}
