package com.reactor.pojo.dto;

import com.reactor.pojo.model.RoleModel;
import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Builder
public class RoleDto {
    private final Long id;
    private final String name;
    private final List<PermissionDto> permissionDtoList;

    public static RoleDto fromModel(final RoleModel model) {
        var permissionDtoList = model.getPermissions().stream()
                .map(permissionModel -> PermissionDto.fromModel(permissionModel))
                .collect(Collectors.toList());
        return RoleDto.builder()
                .id(model.getId())
                .name(model.getName())
                .permissionDtoList(permissionDtoList)
                .build();
    }
}
