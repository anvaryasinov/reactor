package com.reactor.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("role_permission_mappings")
@Getter
@Setter
@Builder
public class RolePermissionMappingEntity extends BaseEntity {

    @Column("role_id")
    private Long roleId;

    @Column("permission_id")
    private Long permissionId;

}
