package com.reactor.service.facade.impl;

import com.reactor.common.TimestampProvider;
import com.reactor.entity.PermissionEntity;
import com.reactor.entity.RoleEntity;
import com.reactor.entity.RolePermissionMappingEntity;
import com.reactor.entity.UserEntity;
import com.reactor.pojo.model.PermissionModel;
import com.reactor.pojo.model.RoleModel;
import com.reactor.pojo.model.UserModel;
import com.reactor.repository.PermissionRepository;
import com.reactor.repository.RolePermissionMappingRepository;
import com.reactor.repository.RoleRepository;
import com.reactor.repository.UserRepository;
import com.reactor.service.facade.UserService;
import com.reactor.service.facade.common.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import reactor.util.function.Tuple2;
import reactor.util.function.Tuple3;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultUserService extends DefaultBaseService<UserEntity, UserModel> implements UserService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PermissionRepository permissionRepository;

    private final RolePermissionMappingRepository rolePermissionMappingRepository;

    @Autowired
    public DefaultUserService(final TimestampProvider timestampProvider,
                              final UserRepository userRepository,
                              final RoleRepository roleRepository,
                              final PermissionRepository permissionRepository,
                              final RolePermissionMappingRepository rolePermissionMappingRepository) {
        super(timestampProvider, userRepository);
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.permissionRepository = permissionRepository;
        this.rolePermissionMappingRepository = rolePermissionMappingRepository;
    }

    @Override
    public Mono<UserModel> getModel(final Long id) {
        return userRepository.findById(id)
                .flatMap(this::fetchWidthRole)
                .map(this::generateModel);
    }

    @Override
    public Mono<UserModel> saveOrUpdate(final UserEntity entity) {
        return super.commitEntity(entity)
                .flatMap(this::fetchWidthRole)
                .map(this::generateModel);
    }

    private Mono<Tuple3<UserEntity, RoleEntity, List<PermissionEntity>>> fetchWidthRole(final UserEntity userEntity) {
        var roleEntityMono = roleRepository.findById(userEntity.getRoleId());
        var userEntityMono = Mono.just(userEntity);
        return Mono.zip(userEntityMono, roleEntityMono)
                .flatMap(this::fetchWidthPermission);
    }

    private Mono<Tuple3<UserEntity, RoleEntity, List<PermissionEntity>>> fetchWidthPermission(
            final Tuple2<UserEntity, RoleEntity> tuple
    ) {
        var permissionEntityListMono =
                rolePermissionMappingRepository.findByRoleId(tuple.getT2().getId())
                        .map(RolePermissionMappingEntity::getRoleId)
                        .flatMap(permissionRepository::findById)
                        .collectList();
        var roleEntityMono = Mono.just(tuple.getT2());
        var userEntityMono = Mono.just(tuple.getT1());
        return Mono.zip(userEntityMono, roleEntityMono, permissionEntityListMono);
    }

    private UserModel generateModel(
            final Tuple3<UserEntity, RoleEntity, List<PermissionEntity>> tuple
    ) {
        var userEntity = tuple.getT1();
        var roleEntity = tuple.getT2();
        var permissionEntitySet = tuple.getT3();
        var permissionModelSet = permissionEntitySet.stream()
                .map(permissionEntity ->
                        PermissionModel.builder()
                                .id(permissionEntity.getId())
                                .name(permissionEntity.getName()).build())
                .collect(Collectors.toSet());
        var roleModel = RoleModel.builder()
                .id(roleEntity.getId())
                .name(roleEntity.getName())
                .permissions(permissionModelSet)
                .build();
        return UserModel.builder()
                .id(userEntity.getId())
                .name(userEntity.getName())
                .role(roleModel)
                .build();
    }

}
