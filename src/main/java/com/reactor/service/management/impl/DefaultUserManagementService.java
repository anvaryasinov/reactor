package com.reactor.service.management.impl;

import com.reactor.entity.UserEntity;
import com.reactor.pojo.dto.UserDto;
import com.reactor.pojo.request.UserRequest;
import com.reactor.pojo.response.UserResponse;
import com.reactor.service.facade.RoleService;
import com.reactor.service.facade.UserService;
import com.reactor.service.management.UserManagementService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
@AllArgsConstructor
public class DefaultUserManagementService implements UserManagementService {

    private final UserService userService;

    private final RoleService roleService;

    @Override
    public Mono<UserResponse> get(final Long id) {
        return userService.getModel(id)
                .map(UserDto::fromModel)
                .map(UserResponse::new);
    }

    @Override
    public Mono<UserResponse> add(final UserRequest request) {
        return roleService.findById(request.getRoleId())
                .map(roleModel ->
                        UserEntity.builder()
                                .name(request.getUserDto().getName())
                                .roleId(roleModel.getId())
                                .build())
                .flatMap(userService::saveOrUpdate)
                .map(UserDto::fromModel)
                .map(UserResponse::new);
    }

    @Override
    public Mono<UserResponse> edit(final UserRequest request) {
        var userDto = request.getUserDto();
        return roleService.findById(request.getRoleId())
                .flatMap(roleEntity -> userService.findById(request.getUserDto().getId())
                        .map(userEntity -> {
                            userEntity.setName(userDto.getName());
                            userEntity.setRoleId(roleEntity.getId());
                            return userEntity;
                        })
                        .flatMap(userService::saveOrUpdate))
                .map(UserDto::fromModel)
                .map(UserResponse::new);
    }

    @Override
    public Mono<Void> remove(final Long id) {
        return userService.deleteById(id);
    }


}
