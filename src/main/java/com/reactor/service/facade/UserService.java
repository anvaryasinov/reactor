package com.reactor.service.facade;

import com.reactor.entity.UserEntity;
import com.reactor.pojo.model.UserModel;
import com.reactor.service.facade.common.BaseService;
import reactor.core.publisher.Mono;

public interface UserService extends BaseService<UserEntity, UserModel> {
    Mono<UserEntity> findById(Long id);
    Mono<UserModel> getModel(Long id);
}
