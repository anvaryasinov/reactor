package com.reactor.controller;

import com.reactor.client.UserClient;
import com.reactor.pojo.request.UserRequest;
import com.reactor.pojo.response.UserResponse;
import com.reactor.service.management.UserManagementService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserClient userClient;

    private final UserManagementService userManagementService;

    @GetMapping(value = "/{id}")
    public Mono<UserResponse> get(@PathVariable(value = "id") final Long id) {
        return userManagementService.get(id);
    }

    @PostMapping
    public Mono<UserResponse> add(@RequestBody final UserRequest request) {
        return userManagementService.add(request);
    }

    @PutMapping
    public Mono<UserResponse> edit(@RequestBody final UserRequest request) {
        return userManagementService.edit(request);
    }

    @DeleteMapping(value = "/{id}")
    public Mono<Void> remove(@PathVariable(value = "id") final Long id) {
        return userManagementService.remove(id);
    }

    @GetMapping(value = "/test")
    public Mono<String> testClient() {
        return userClient.getUserData();
    }

}
