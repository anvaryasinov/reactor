package com.reactor.pojo.model;

import lombok.Builder;
import lombok.Getter;

import java.util.Set;

@Getter
@Builder
public class RoleModel {
    private final Long id;
    private final String name;
    private final Set<PermissionModel> permissions;
}
