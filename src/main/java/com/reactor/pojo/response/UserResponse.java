package com.reactor.pojo.response;

import com.reactor.pojo.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserResponse {
    private final UserDto userDto;
}
