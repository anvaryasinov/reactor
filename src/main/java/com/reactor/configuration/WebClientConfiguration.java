package com.reactor.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
public class WebClientConfiguration {

    private final String userServerUrl;

    @Autowired
    public WebClientConfiguration(@Value("${user-server-url}") final String userServerUrl) {
        this.userServerUrl = userServerUrl;
    }

    @Bean
    @Qualifier("userWebClient")
    public WebClient userWebClient() {
        return WebClient.create(this.userServerUrl);
    }

}
