package com.reactor.common;

import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
public class TimestampProvider {

    public ZonedDateTime getServerZonedDateTime() {
        return ZonedDateTime.now();
    }

}

