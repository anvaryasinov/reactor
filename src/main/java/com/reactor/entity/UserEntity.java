package com.reactor.entity;

import lombok.*;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

@Table("users")
@Getter
@Setter
@Builder
public class UserEntity extends BaseEntity {

    @Column("name")
    private String name;

    @Column("role_id")
    private Long roleId;

}
