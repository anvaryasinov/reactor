package com.reactor.service.facade.impl;

import com.reactor.common.TimestampProvider;
import com.reactor.entity.PermissionEntity;
import com.reactor.entity.RoleEntity;
import com.reactor.entity.RolePermissionMappingEntity;
import com.reactor.pojo.model.PermissionModel;
import com.reactor.pojo.model.RoleModel;
import com.reactor.repository.PermissionRepository;
import com.reactor.repository.RolePermissionMappingRepository;
import com.reactor.repository.RoleRepository;
import com.reactor.service.facade.RoleService;
import com.reactor.service.facade.common.DefaultBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DefaultRoleService extends DefaultBaseService<RoleEntity, RoleModel> implements RoleService {

    private final RoleRepository roleRepository;

    private final PermissionRepository permissionRepository;

    private final RolePermissionMappingRepository rolePermissionMappingRepository;

    @Autowired
    public DefaultRoleService(final TimestampProvider timestampProvider,
                              final RoleRepository roleRepository,
                              final PermissionRepository permissionRepository,
                              final RolePermissionMappingRepository rolePermissionMappingRepository) {
        super(timestampProvider, roleRepository);
        this.roleRepository = roleRepository;
        this.permissionRepository = permissionRepository;
        this.rolePermissionMappingRepository = rolePermissionMappingRepository;
    }

    @Override
    public Mono<RoleModel> getModel(final Long id) {
        return roleRepository.findById(id)
                .flatMap(this::fetchWidthPermission)
                .map(this::generateModel);

    }

    @Override
    public Mono<RoleModel> saveOrUpdate(final RoleEntity entity) {
        return super.commitEntity(entity)
                .flatMap(this::fetchWidthPermission)
                .map(this::generateModel);
    }

    private Mono<Tuple2<RoleEntity, List<PermissionEntity>>> fetchWidthPermission(final RoleEntity roleEntity) {
        var permissionEntityListMono =
                rolePermissionMappingRepository.findByRoleId(roleEntity.getId())
                        .map(RolePermissionMappingEntity::getPermissionId)
                        .flatMap(permissionRepository::findById)
                        .collectList();
        var roleEntityMono = Mono.just(roleEntity);
        return Mono.zip(roleEntityMono, permissionEntityListMono);
    }

    private RoleModel generateModel(final Tuple2<RoleEntity, List<PermissionEntity>> tuple) {
        var roleEntity = tuple.getT1();
        var permissionEntitySet = tuple.getT2();

        var permissionModelSet = permissionEntitySet.stream()
                .map(permissionEntity ->
                        PermissionModel.builder()
                                .id(permissionEntity.getId())
                                .name(permissionEntity.getName())
                                .build())
                .collect(Collectors.toSet());

        return RoleModel.builder()
                .id(roleEntity.getId())
                .name(roleEntity.getName())
                .permissions(permissionModelSet)
                .build();
    }

}
