package com.reactor.service.facade;

import com.reactor.entity.RoleEntity;
import com.reactor.pojo.model.RoleModel;
import com.reactor.service.facade.common.BaseService;
import reactor.core.publisher.Mono;

public interface RoleService extends BaseService<RoleEntity, RoleModel> {
    Mono<RoleModel> getModel(Long id);
}
