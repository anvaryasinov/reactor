package com.reactor.pojo.request.role;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
public class CreateRoleRequest {
    private final String name;
    private final Long roleId;
}
